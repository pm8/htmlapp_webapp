# jsCMS
## What is this?
jsCMS is a Contents Management System for Node.js with Express4 and ~~MongoDB~~ (DISABLED!!) .
and render by json file.
written pug.

## Required
```
jscms 
├── body-parser@1.17.1
├── child_process@1.0.2
├── express@4.15.2
├── express-generator@4.15.0
├── method-override@2.3.8
├── morgan@1.8.1
├── node-sass@3.13.1
├── node-zip@1.1.1
├── pug@2.0.0-beta9
├── riot@2.6.8
└── uglify-js@2.8.22
```

## install
### CentOS 7
#### install nodeJS

```
curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
yum -y install nodejs
```

https://nodejs.org/en/download/package-manager/

#### clone project

```
cd /path/to/put/project
git clone https://bitbucket.org/pm8/jscms.git
```

#### install required files
##### install from npm

```
cd /path/to/project/directory
npm i
npm install nodemon -g
```


### macOS
#### install nodeJS
visit https://nodejs.org/en/download/ and download mac installer. then install.

OR

if you have already installed homebrew, type like this

```
brew install nodebrew
nodebrew install-binary v6.9.1
```

http://brew.sh/
https://github.com/Homebrew/homebrew-core/tree/master/Formula

#### clone project

```
cd /path/to/put/project
git clone https://bitbucket.org/pm8/jscms.git
```

#### install required files
##### install from npm

```
cd /path/to/project/directory
npm i
npm install nodemon -g
```

## Directory Tree by default
- raw/
    - css/
        - (SCSS Files)
    - js/
        - (JavaScript Files)
    - pug/
        - (Pug Files)
    - tag/
        - (Tag Files)
- routes/
    - (node routes Files)
- static/
*NOTE: here (under the `/static`) will be document root. You can place other files such as images here if you need. <Of-course, for example, you can also place Javascript Files which won't Uglify under `lib/js` or/and CSS Files under `lib/css`.>*
    - lib/
        - css/
            - (Compiled CSS Files will place here)
        - js/
            - (Compiled JavaScript Files will place here)
        - tag/
            - (Compiled Tag Files will place here)
        - recipes.json *(will be generate when access `/refresh`)*
        

## Usage by default
### put raw files
put `.scss` files in `/raw/css` (NOT scss), `.js` files in `/raw/js`,and `.pug` files in `/raw/pug`.

### start project

```
cd /path/to/project/directory
nodemon server.js
```

project open localhost:80 by default.
if you need to change something, edit `/setting.js`.


## functions
