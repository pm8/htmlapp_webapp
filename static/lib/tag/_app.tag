
riot.tag2('app', '<header if="{now.header.isHeader}" data-is="global-header" dose="{dose}"></header> <section if="{now.main.isMain}" data-is="global-main" dose="{dose}"></section> <nav if="{now.menu.isMenu}" data-is="global-menu" dose="{dose}"></nav>', '', '', function(opts) {
    const self=this;
    this.one('before-mount', function () {
    	fetch('/lib/data/dose.json', {
    		method: 'GET'
    	}).then(function (res) {
    		return res.json();
    	}).then(function (data) {
    		self.dose = data;
    		self.doseData = self.dose.data;
    		self.update();
    	});
    });
});