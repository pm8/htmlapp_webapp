
riot.tag2('index', '<section id="list" data-ctrl="list"> <virtual each="{item,i in doseData}"> <h2 if="{isSameMonth(item.time)}">{_utility_.extractDate(new Date(item.time), \'YYYYMM\', \'japanese\')}</h2> <h3 if="{isSameDate(item.time)}">{_utility_.extractDate(new Date(item.time), \'DD\', \'japanese\')}</h3> <article class="data {_utility_.extractDate(new Date(item.time), \'YYYYMMDD\', \'hyphenation\')}" data-time="{_utility_.extractDate(new Date(item.time), \'YYYYMMDDhhmmss\', \'none\')}" onclick="{clickDetail}"> <header> <h4>{_utility_.extractDate(new Date(item.time), \'hhmmss\', \'slash\')}</h4> </header> <section class="detail hide" onclick="{clickDetail}"> <p>{item.comment}</p> <table> <tr each="{condition, j in item.conditions}"> <th>{condition.type}</th> <td>{condition.value} {condition.unit}</td> </tr> </table> </section> </article> </virtual> </section>', '', '', function(opts) {
    const self = this;
    this.isSameMonth=function(e){
    	var prevDate=window.sessionStorage.getItem('isSameMonthPrevDate')?new Date(window.sessionStorage.getItem('isSameMonthPrevDate')):new Date(0);
    	var eDate= new Date(e);
    	var eYYYYMM=_utility_.extractDate(eDate,'YYYYMM','none');
    	var nowYYYYMM=_utility_.extractDate(prevDate,'YYYYMM','none');
    	window.sessionStorage.setItem('isSameMonthPrevDate',eDate);
    	if(eYYYYMM!=nowYYYYMM){
    		return true;
    	}else{
    		return false;
    	}
    };
    this.isSameDate = function (e) {
    	var prevDate = window.sessionStorage.getItem('isSameDatePrevDate') ? new Date(window.sessionStorage.getItem('isSameDatePrevDate')) : new Date(0);
    	var eDate = new Date(e);
    	var eYYYYMMDD = _utility_.extractDate(eDate, 'YYYYMMDD', 'none');
    	var nowYYYYMMDD = _utility_.extractDate(prevDate, 'YYYYMMDD', 'none');
    	window.sessionStorage.setItem('isSameDatePrevDate', eDate);
    	if (eYYYYMMDD != nowYYYYMMDD) {
    		return true;
    	} else {
    		return false;
    	}
    };
    this.clickDetail = function (e) {
    	var t=e.currentTarget.getElementsByTagName('section')[0];
    	t.classList.toggle('hide');
    };
    this.one('updated', function () {
    	self.doseData = opts.appopts.dose.data;
    	self.update();
    });
});