/**! @license settings.js; JavaScript for jsCMS by Yuki TANABE on May 12, 2017 | pm8.jp */

//hostSettings
exports.port=8080;
exports.host='localhost';

//keyName Setting, (almost) equal to each directory name
exports.pugKeyName = 'pug';
exports.routesKeyName = 'routes';
exports.rawKeyName='raw';
exports.staticKeyName = 'static';
exports.libKeyName = 'lib';
exports.cssKeyName = 'css';
exports.scssKeyName = 'scss'; // but scss directory is named 'css' by default
exports.jsKeyName = 'js';
exports.tagKeyName = 'tag';
exports.htmlExName='html';

//URL settings, from '/'.
exports.URL={
	index:'/',
};

//maps
exports.path={};
exports.rawPath={};
exports.staticPath={};
exports.path[exports.rawKeyName]=__dirname+'/'+exports.rawKeyName;
exports.path[exports.routesKeyName]=__dirname+'/'+exports.routesKeyName;
exports.path[exports.staticKeyName]=__dirname+'/'+exports.staticKeyName;
exports.rawPath[exports.pugKeyName]=exports.path[exports.rawKeyName]+'/'+exports.pugKeyName;
exports.rawPath[exports.scssKeyName]=exports.path[exports.rawKeyName]+'/'+exports.cssKeyName; // scss directory name is 'css' by default.
exports.rawPath[exports.tagKeyName]=exports.path[exports.rawKeyName]+'/'+exports.tagKeyName;
exports.rawPath[exports.jsKeyName]=exports.path[exports.rawKeyName]+'/'+exports.jsKeyName;
exports.staticPath[exports.libKeyName]=exports.path[exports.staticKeyName]+'/'+exports.libKeyName;
exports.staticPath[exports.cssKeyName]=exports.staticPath[exports.libKeyName]+'/'+exports.cssKeyName;
exports.staticPath[exports.tagKeyName]=exports.staticPath[exports.libKeyName]+'/'+exports.tagKeyName;
exports.staticPath[exports.jsKeyName]=exports.staticPath[exports.libKeyName]+'/'+exports.jsKeyName;


//initialize and set more exports
const fs = require('fs');
const pug = require('pug');
const riot = require('riot');
const sass = require('node-sass');
const uglify = require("uglify-js");
const reg=/(.*)(?:\.([^.]+$))/; // for detect file names
const regURL = '.+/(.+?)\.[a-z]+([\?#;].*)?$';
const licenseRegexp = /^\!|^@preserve|^@cc_on|\bMIT\b|\bMPL\b|\bGPL\b|\(c\)|License|Copyright/mi;
const http = require('http');
var scssFiles = fs.readdirSync(exports.rawPath[exports.scssKeyName]);

for(let path in exports.staticPath){
	fs.mkdir(exports.staticPath[path],function(err){
		if(err){
			if(err.code!='EEXIST'){
				console.log(err);
				return 1;
			}
		}else{
			console.log('directory: '+exports.staticPath[path]+' is made.')
		}
	});
}

for(let i=0; i<scssFiles.length; i++){
	compileScss(scssFiles[i]);
}
var rawTagFiles = fs.readdirSync(exports.rawPath[exports.tagKeyName]);
for(let i=0; i<rawTagFiles.length; i++){
	compileTag(rawTagFiles[i]);
}
var routesFiles = fs.readdirSync(exports.path[exports.routesKeyName]);
var pugFiles = fs.readdirSync(exports.rawPath[exports.pugKeyName]);
var jsFiles  = fs.readdirSync(exports.rawPath[exports.jsKeyName]);

for(let i=0; i<pugFiles.length;i++) {
	let t=exports.rawPath[exports.pugKeyName] + '/'+pugFiles[i];
	let res = pug.compileFile(t);
	fs.writeFileSync(exports.path[exports.staticKeyName]+'/'+t.match(regURL)[1]+'.'+exports.htmlExName,res({}));
}
for(let i=0; i<jsFiles.length;i++){
	var js=compressJs(exports.rawPath[exports.jsKeyName], jsFiles[i]);
	fs.writeFile(exports.staticPath[exports.jsKeyName]+'/'+jsFiles[i].match(reg)[1]+'.min.'+jsFiles[i].match(reg)[2], js.code, function (err) {
		if(err){
			console.log(err);
		}
	});
	fs.writeFile(exports.staticPath[exports.jsKeyName]+'/'+jsFiles[i].match(reg)[1]+'.min.map', js.map, function (err) {
		if(err){
			console.log(err);
		}
	});
}
var tagFiles = fs.readdirSync(exports.staticPath[exports.tagKeyName]);
var cssFiles = fs.readdirSync(exports.staticPath[exports.cssKeyName]);
var maps={};
maps[exports.scssKeyName]=scssFiles;
maps[exports.cssKeyName]=cssFiles;
maps[exports.pugKeyName]=pugFiles;
maps[exports.routesKeyName]=routesFiles;
maps[exports.tagKeyName]=tagFiles;
for(let prop in maps){
	if(!exports[prop]){
		exports[prop]={};
	}
	mapize(maps[prop],exports[prop]);
}
for(let prop in exports[exports.routesKeyName]){
	exports[exports.routesKeyName][prop]=exports.path[exports.routesKeyName]+'/'+exports[exports.routesKeyName][prop];
}
console.log(exports);


//functions
function compressJs(dir,t){
	var isLicenseComment = (function() {//check license line
		var _prevCommentLine = 0;
		return function(node, comment) {
			if (licenseRegexp.test(comment.value) || comment.line === 1 || comment.line === _prevCommentLine + 1) {
				_prevCommentLine = comment.line;
				return true;
			}
			_prevCommentLine = 0;
			return false;
		};
	})();
	return uglify.minify([dir+'/'+t], {
		compress: {
			dead_code: true,
			drop_debugger : false, // remove debugger; statements
			global_defs: {
				DEBUG: true
			},
			hoist_vars:true
		},
		output: {
			comments: isLicenseComment
		},
		outSourceMap: t.match(reg)[1]+'.min.map'
	});
}
function compileScss(t){
	sass.render({
		file:exports.rawPath[exports.scssKeyName]+'/'+t,
		outputStyle: 'compressed'
	},function (error,result) {
		if (error) {
			console.log('SCSS COMPILE ERROR=================================================');
			console.log(error.status);
			console.log(error.column);
			console.log(error.message);
			console.log(error.line);
			console.log('=================================================SCSS COMPILE ERROR');
		}
		else {
			fs.writeFileSync(exports.staticPath[exports.cssKeyName]+'/'+t.match(reg)[1]+'.css',result[exports.cssKeyName].toString());
		}
	});
}
function compileTag(t){
	riot.parsers[exports.cssKeyName].sass = function (tagName, css) {
		var result = sass.renderSync({data: css});
		return result[exports.cssKeyName].toString();
	};
	var code = pug.compileFile(exports.rawPath[exports.tagKeyName]+'/'+t,{'pretty':true});
	var js = riot.compile(code({}));
	fs.writeFile(exports.staticPath[exports.tagKeyName]+'/'+t.match(reg)[1]+'.'+t.match(reg)[2], js);
}
function mapize(a,b){
	for(let i=0; i<a.length;i++){
		b[a[i].match(reg)[1]] = a[i].match(reg)[0];
	}
}