/**! @license get.js; JavaScript for jsCMS by Yuki TANABE on May 12, 2017 | pm8.jp */

const settings = require('../settings.js');

exports.index= function(req,res){
	res.render(settings.pug['index'],{});
};
