/**! @license server.js; JavaScript for jsCMS by Yuki TANABE on May 12, 2017 | pm8.jp */

const express = require('express');
const app = express();
const logger = require('morgan');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const settings = require('./settings.js');
const routes = settings.routes;
const get = require(routes['get']);
const post = require(routes['post']);

app.set('view engine', 'pug');
app.set('views', settings.rawPath['pug']);

app.locals.pretty = false; //output one-line html from pug file when false.


//middleware
app.use(logger('dev')); //logging for dev
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride()); //middleware for such as POST, PUT and DELETE.
app.use(express.static(settings.path['static'])); //find static files with directory and file name(s).

////get
app.get(settings.URL['index'], get.index);

////post


app.use(function(req, res, next){ //404 error
	res.status(404);
	// res.render(settings.pug['notfound']);
	res.render(settings.pug['index'],{});
});
app.use(function(err, req, res, next){ //500 error
	res.status(500);
	res.render(settings.pug['servererr'],{err:err});
});

app.listen(settings.port);
console.log('start nodeServer on '+settings.host+':'+settings.port);