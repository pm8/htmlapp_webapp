//- @license app.tag; PugTemplate for jsCMS by Yuki TANABE on May 12, 2017 | pm8.jp
app
	header(if="{now.header.isHeader}" data-is="global-header" dose="{dose}")
	section(if="{now.main.isMain}" data-is="global-main" dose="{dose}")
	nav(if="{now.menu.isMenu}" data-is="global-menu" dose="{dose}")
	script.
		const self=this;
		this.one('before-mount', function () {
			fetch('/lib/data/dose.json', {
				method: 'GET'
			}).then(function (res) {
				return res.json();
			}).then(function (data) {
				self.dose = data;
				self.doseData = self.dose.data;
				self.update();
			});
		});