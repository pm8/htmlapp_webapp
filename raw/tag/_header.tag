//- @license header.tag; PugTemplate for jsCMS by Yuki TANABE on May 12, 2017 | pm8.jp
global-header#header
	h1 デバイス： {id}
	script.
		const self=this;
		this.one('updated', function () {
			self.id = opts.dose.id;
			self.update();
		});