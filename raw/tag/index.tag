//- @license index.tag; PugTemplate for jsCMS by Yuki TANABE on May 12, 2017 | pm8.jp
index
	section#list(data-ctrl='list')
		virtual(each='{item,i in doseData}')
			h2(if="{isSameMonth(item.time)}") {_utility_.extractDate(new Date(item.time), 'YYYYMM', 'japanese')}
			h3(if="{isSameDate(item.time)}") {_utility_.extractDate(new Date(item.time), 'DD', 'japanese')}
			article(data-time="{_utility_.extractDate(new Date(item.time), 'YYYYMMDDhhmmss', 'none')}" class="data {_utility_.extractDate(new Date(item.time), 'YYYYMMDD', 'hyphenation')}" onclick="{clickDetail}")
				header
					h4 {_utility_.extractDate(new Date(item.time), 'hhmmss', 'slash')}
				section.detail.hide(onclick="{clickDetail}")
					p {item.comment}
					table
						tr(each="{condition, j in item.conditions}")
							th {condition.type}
							td {condition.value} {condition.unit}

	script.
		const self = this;
		this.isSameMonth=function(e){
			var prevDate=window.sessionStorage.getItem('isSameMonthPrevDate')?new Date(window.sessionStorage.getItem('isSameMonthPrevDate')):new Date(0);
			var eDate= new Date(e);
			var eYYYYMM=_utility_.extractDate(eDate,'YYYYMM','none');
			var nowYYYYMM=_utility_.extractDate(prevDate,'YYYYMM','none');
			window.sessionStorage.setItem('isSameMonthPrevDate',eDate);
			if(eYYYYMM!=nowYYYYMM){
				return true;
			}else{
				return false;
			}
		};
		this.isSameDate = function (e) {
			var prevDate = window.sessionStorage.getItem('isSameDatePrevDate') ? new Date(window.sessionStorage.getItem('isSameDatePrevDate')) : new Date(0);
			var eDate = new Date(e);
			var eYYYYMMDD = _utility_.extractDate(eDate, 'YYYYMMDD', 'none');
			var nowYYYYMMDD = _utility_.extractDate(prevDate, 'YYYYMMDD', 'none');
			window.sessionStorage.setItem('isSameDatePrevDate', eDate);
			if (eYYYYMMDD != nowYYYYMMDD) {
				return true;
			} else {
				return false;
			}
		};
		this.clickDetail = function (e) {
			var t=e.currentTarget.getElementsByTagName('section')[0];
			t.classList.toggle('hide');
		};
		this.one('updated', function () {
			self.doseData = opts.appopts.dose.data; //headerとfooterとは一段階階層が変わる。
			self.update();
		});