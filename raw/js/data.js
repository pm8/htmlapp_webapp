/**! @license data.js; JavaScript for jsCMS by Yuki TANABE on May 12, 2016 | pm8.jp */
var data={
	index:{
		header:{
			isHeader:true
		},
		main:{
			isMain:true,
			type:"index"
		},
		menu:{
			isMenu:true
		}
	}
};