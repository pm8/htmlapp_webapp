/**
 * Created by nobu on 2016/12/02.
 */

var AppBridge = (function () {
    //ネイティブからのコールバック関数を格納する
    var callbackFunctions_ = {};

    //コンストラクタ
    var AppBridge = function()  {
    };

    var proto = AppBridge.prototype;

    proto.deviceId = "";
    proto.uniquId = "";
    proto.osName = "";
    proto.osVersion = "";
    proto.appliVersion = "";
    proto.token = "";
    proto.debug = false;
    proto.initial = false;

    /**
     * コールバック関数の登録
     */
    proto.push = function(callback) {
        var key = (new Date()).getTime();
        callbackFunctions_[key] = callback;
        return key;
    };

    /**
     * コールバック関数の実行
     */
    proto.pop = function(key, data) {
        var callback = callbackFunctions_[key];
        if(data != null) {
            callback(data);
        }else{
            callback();
        }
        callbackFunctions_[key] = null;
    };

    /**
     * 設定画面表示
     */
    proto.displaySetting = function(url) {
        location.href = "setting://localhost";
    };

    /**
     * モーダル表示
     */
    proto.displayModal = function(url) {
        if(url.indexOf("http") == 0) {
            location.href = "bottominbrowser"+url.substring(4);
        }else{
            location.href = "bottominbrowser://localhost"+url;
        }
    };

    /**
     * モーダル非表示
     */
    proto.dissmissModal = function() {
        location.href = "pop://localhost";
    };

    /**
     * ページ遷移
     */
    proto.displayView = function(url) {
        if(url.indexOf("http") == 0) {
            location.href = "rightinbrowser"+url.substring(4);
        }else{
            location.href = "rightinbrowser://localhost"+url;
        }
    };

    /**
     * ページバック
     */
    proto.backView = function() {
        location.href = "dissmiss://localhost";
    };

    /**
     * ローカル通知追加
     */
    proto.addNotification = function(storageId, title, text, subText, date, callback) {
        var key = this.push(callback);
        location.href = "notification://localhost/add/?key="+key+"&stId="+encodeURIComponent(storageId)+"&title="+encodeURIComponent(title)+"&text="+encodeURIComponent(text)+"&subtext="+encodeURIComponent(subText)+"&date="+encodeURIComponent(date);
    };

    /**
     * ローカル通知追加
     */
    proto.addNotificationRepeat = function(storageId, title, text, subText, date, repeat, callback) {
        var key = this.push(callback);
        location.href = "notification://localhost/add/?key="+key+"&stId="+encodeURIComponent(storageId)+"&title="+encodeURIComponent(title)+"&text="+encodeURIComponent(text)+"&subtext="+encodeURIComponent(subText)+"&date="+encodeURIComponent(date)+"&repeat="+repeat;
    };

    /**
     * ローカル通知変更
     */
    proto.modNotification = function(notificationId, storageId, title, text, subText, date, callback) {
        var key = this.push(callback);
        location.href = "notification://localhost/mod/?key="+key+"&id="+encodeURIComponent(notificationId)+"&stId="+encodeURIComponent(storageId)+"&title="+encodeURIComponent(title)+"&text="+encodeURIComponent(text)+"&subtext="+encodeURIComponent(subText)+"&date="+encodeURIComponent(date)+"&repeat=false";
    };

    /**
     * ローカル通知変更
     */
    proto.modNotificationRepeat = function(notificationId, storageId, title, text, subText, date, repeat, callback) {
        var key = this.push(callback);
        location.href = "notification://localhost/mod/?key="+key+"&id="+encodeURIComponent(notificationId)+"&stId="+encodeURIComponent(storageId)+"&title="+encodeURIComponent(title)+"&text="+encodeURIComponent(text)+"&subtext="+encodeURIComponent(subText)+"&date="+encodeURIComponent(date)+"&repeat="+repeat;
    };

    /**
     * ローカル通知削除
     */
    proto.delNotification = function(notificationId, callback) {
        var key = this.push(callback);
        location.href = "notification://localhost/del/?key="+key+"&id="+encodeURIComponent(notificationId);
    };

    /**
     * ローカル通知取得：関連するイベント
     */
    proto.getNotificationWithStorageIds = function(storageIds, callback) {
        var key = this.push(callback);
        var sql = storageIds.length > 1 ?
                    "select * from local_notification where delete_flg <> 1 and local_storage_id in ("+storageIds.join(",")+");" :
                    "select * from local_notification where delete_flg <> 1 and local_storage_id in ("+storageIds[0]+");";
        var ids = storageIds.length > 1 ? storageIds.join(",") : storageIds[0];
        location.href = "notification://localhost/get/?key="+key+"&sql="+encodeURIComponent(sql)+"&ids="+ids;
    };

    /**
     * ローカル通知取得：IDリスト
     */
    proto.getNotificationWithNotificationIds = function(notificationIds, callback) {
        var key = this.push(callback);
        var sql = notificationIds.length > 1 ?
                    "select * from local_notification where id in ("+notificationIds.join(",")+");" :
                    "select * from local_notification where id in ("+notificationIds[0]+");";
        var ids = notificationIds.length > 1 ? notificationIds.join(",") : notificationIds[0];
        location.href = "notification://localhost/get/?key="+key+"&sql="+encodeURIComponent(sql)+"&ids="+ids;
    };

    // 画面生成時に呼ぶ

    // テンプレ
    // proto.gatrack = function（スクリーン名, callback) {

    /**
     * GAトラックング
     */
    proto.gatrack = function(title, callback) {
        var key = this.push(callback);
        location.href = "gatrack://localhost/?key="+key+"&title="+encodeURIComponent(title);
    };

    // コールバックは空で構わない
    // ボタン（固定）は後で変更
    // 画面遷移する時に遷移前に呼ぶ、その後に遷移する（クロージャーのイメージ）

    // テンプレ
    // proto.gaevent = function(ボタン（固定）, タップ（固定）, ボタン名（資料参照）, スクリーン名（資料参照）, callback) {

    /**
     * GAイベント
     */
    proto.gaevent = function(category, action, label, screen, callback) {
        var key = this.push(callback);
        location.href = "gaevent://localhost/?key="+key+"&category="+encodeURIComponent(category)+"&action="+encodeURIComponent(action)+"&label="+encodeURIComponent(label)+"&screen="+encodeURIComponent(screen);
    };

    /**
     * ローカルDB
     */
    proto.dbaccess = function(sql, callback) {
        var key = this.push(callback);
        location.href = "dbaccess://localhost/?key="+key+"&sql="+encodeURIComponent(sql);
    };

    /**
     * ネイティブ通信
     */
    proto.ajax = function(url, method, data, callback) {
        if(_utility_.isApp()){
            var key = this.push(callback);
            location.href = "ajax://localhost/?key="+key+"&url="+encodeURIComponent(url)+"&method="+method+"&data="+encodeURIComponent(data);
        }else {
            if (self.fetch) {
                var para = {};
                para['mode'] = 'cros';
                para['method'] = method ? method : 'GET';
                if (data && method != 'GET') {
                    para['body'] = data;
                }
                fetch(url, para).then(function (res) {
                    return res.text();
                }).then(function (data) {
                    callback(data);
                }).catch(function (err) {
                    console.log(err);
                });
            } else {
                var r = new XMLHttpRequest();
                var usr = ''; //BasicAuth
                var pw = ''; //BasicAuth
                r.onreadystatechange = function () {
                    switch (r.readyState) {
                        case 0:
                            console.log('uninitialized.')
                            break;
                        case 1:
                        case 2:
                            console.log('loading...');
                            break;
                        case 3:
                            console.log('interactive... ' + r.responseText.length + ' bytes.');
                            break;
                        case 4:
                            if (r.status == 200 || r.status == 304) {
                                var data = {result: 200};
                                data['data'] = JSON.parse(r.responseText);
                                callback(JSON.stringify(data));
                            } else {
                                console.log('Failed. HttpStatus: ' + r.statusText);
                            }
                            break;
                    }
                };
                r.open(method, url, true, usr, pw);
                r.send(null);
            }
        }
    };

    proto.ajaxWithHeader = function(url, method, header, data, callback) {
        var key = this.push(callback);
        location.href = "ajax://localhost/?key="+key+"&url="+encodeURIComponent(url)+"&method="+method+"&data="+encodeURIComponent(data)+"&header="+encodeURIComponent(header);
    };

    /**
     * alert表示
     */
    proto.alert = function(title, message, buttonName, callback) {
        var key = this.push(callback);
        location.href = "alert://localhost/add/?key="+key+"&title="+encodeURIComponent(title)+"&message="+encodeURIComponent(message)+"&buttonName="+encodeURIComponent(buttonName);
    };

    /**
     * alert表示
     */
    proto.confirm = function(title, message, buttonName, buttonCancel, others, callback) {
        var key = this.push(callback);
        location.href = "confirm://localhost/add/?key="+key+"&title="+encodeURIComponent(title)+"&message="+encodeURIComponent(message)+"&buttonName="+encodeURIComponent(buttonName)+"&buttonCancel="+encodeURIComponent(buttonCancel)+"&others="+others;
    };

    /**
     * アクセストークン取得
     */
    proto.getAccessToken = function(callback) {
        var key = this.push(callback);
        location.href = "gettoken://localhost/?key="+key;
    };

    proto.notificationOn = function(callback) {
        var key = this.push(callback);
        location.href = "debugnotitificaitonon://localhost/?key="+key;
    };

    proto.notificationOff = function(callback) {
        var key = this.push(callback);
        location.href = "debugnotitificaitonff://localhost/?key="+key;
    };

    proto.getGoogleToken = function(callback) {
        var key = this.push(callback);
        location.href = "gogltoken://localhost/?key="+key;
    };
    proto.getGoogleToken401 = function(callback) {
        var key = this.push(callback);
        location.href = "gogltoken401://localhost/?key="+key;
    };
    proto.getOutlookToken = function(callback) {
        var key = this.push(callback);
        location.href = "oloktoken://localhost/?key="+key;
    };
    proto.getOutlookToken401 = function(callback) {
        var key = this.push(callback);
        location.href = "oloktoken401://localhost/?key="+key;
    };
    proto.netcheck = function(callback) {
        var key = this.push(callback);
        location.href = "netcheck://localhost/?key="+key;
    };

    return AppBridge;
})();

//グローバル変数
var _bridge_ = new AppBridge();

//グローバル関数
var pop = function(key, data) {
    _bridge_.pop(key, data);
};

//var swipeRight = function() {
//};

//var swipeLeft = function() {
//};

var launchOther = function(path) {
};

var terminateMovie = function() {
};

var setIds = function(deviceId, uniqueId, os, osVer, appVer, debug, initial) {
    _bridge_.deviceId = deviceId;
    _bridge_.uniquId = uniqueId;
    _bridge_.osName = os;
    _bridge_.osVersion = osVer;
    _bridge_.appliVersion = appVer;
    _bridge_.debug = debug != null && debug.length > 0 && debug == "true" ? true : false;
    _bridge_.initial = initial != null && initial.length > 0 && initial == "true" ? true : false;
    console.log("-------------------------------------");
    console.log("device:"+deviceId);
    console.log("unique:"+uniqueId);
    console.log("os:"+os);
    console.log("osVer:"+osVer);
    console.log("appVer:"+appVer);
    console.log("debug:"+debug);
    console.log("-------------------------------------");
};

var setToken = function(token) {

};

window.onerror = function (msg, file, line, column, err) {
    //TODO
};
