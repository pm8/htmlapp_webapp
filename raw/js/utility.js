/**! @license utility.js; JavaScript for jsCMS by Yuki TANABE on May. 12, 2017 | pm8.jp */
var Utility = (function () {
	var Utility = function()  {
	};
	var util = Utility.prototype;
	util.getDateSlash=function(dateObj){
		var dateArr=['日','月','火','水','木','金','土'];
		var d = dateObj;
		var yyyy=String(d.getFullYear());
		var mm=String(d.getMonth()+1);
		var dd=String(d.getDate());
		var dy=dateArr[d.getDay()];

		return yyyy+'/'+mm+'/'+dd+' ('+dy+')'
	};
	util.extractDate=function(dateObj,type,style){
		var d=dateObj;
		var YYYY=d.getFullYear();
		var symbolsHash={
			slash:'/',
			sp:' ',
			colon:':',
			hyphen:'-'
		};
		var MM=('00'+String(d.getMonth()+1)).substr(-2);
		var DD=('00'+String(d.getDate())).substr(-2);
		var hh=('00'+String(d.getHours())).substr(-2);
		var mm=('00'+String(d.getMinutes())).substr(-2);
		var ss=('00'+String(d.getSeconds())).substr(-2);
		var re='';
		switch (style){
			case 'slash':
				YYYY+=symbolsHash.slash;
				MM+=symbolsHash.slash;
				DD+=symbolsHash.sp;
				hh+=symbolsHash.colon;
				mm+=symbolsHash.colon;
				break;
			case 'japanese':
				YYYY+='年';
				MM+='月';
				DD+='日'+symbolsHash.sp;
				hh+='時';
				mm+='分';
				ss+='秒';
				break;
			case 'hyphenation':
				YYYY+=symbolsHash.hyphen;
				MM+=symbolsHash.hyphen;
				DD+=symbolsHash.sp;
				hh+=symbolsHash.hyphen;
				mm+=symbolsHash.hyphen;
				break;
		}
		switch(type){
			case 'YYYYMMDDhhmmss':
				re=YYYY+MM+DD+hh+mm+ss;
				break;
			case 'YYYYMMDD':
				re=YYYY+MM+DD;
				break;
			case 'YYYYMM':
				re=YYYY+MM;
				break;
			case 'DD':
				re=DD;
				break;
			case 'hhmmss':
				re=hh+mm+ss;
				break;
		}
		for(var ig in symbolsHash){
			if(re.slice(-1)===symbolsHash[ig]){
				re=re.substr(0,re.length-1);
			}
		}
		return re;
	};
	return Utility;
})();
var _utility_ = new Utility();